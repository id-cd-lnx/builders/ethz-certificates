%define pkidir %{_sysconfdir}/pki
%define catrustdir %{_sysconfdir}/pki/ca-trust
%define classic_tls_bundle ca-bundle.crt
%define openssl_format_trust_bundle ca-bundle.trust.crt
%define p11_format_bundle ca-bundle.trust.p11-kit
%define legacy_default_bundle ca-bundle.legacy.default.crt
%define legacy_disable_bundle ca-bundle.legacy.disable.crt
%define java_bundle java/cacerts

Summary: The ETH Zurich CA root certificate bundle
Name: ethz-certificates

Version: 2023.1
Release: 1%{?dist}
License: Public Domain

Group: System Environment/Base
URL: http://www.ethz.ch/

Source0: README.ethz-certificates
Source1: ETHZRootCA2020.crt
Source2: ETHZIssuingCA2020.crt
Source3: DigiCertGlobalRootCA.pem
Source4: DigiCertTLSRSASHA2562020CA1-1.pem
Source5: DigiCertGlobalRootG2.pem
Source6: DigiCertGlobalG2TLSRSASHA2562020CA1.pem

BuildArch: noarch

Requires: ca-certificates

%description
This package contains the set of root certificates used
for the CA at ETH Zurich.

%prep
rm -rf %{name}
mkdir %{name}
mkdir %{name}/certs

%build
pushd %{name}/certs
 pwd
 cp %{SOURCE1} .
 cp %{SOURCE2} .
 cp %{SOURCE3} .
 cp %{SOURCE4} .
 cp %{SOURCE5} .
 cp %{SOURCE6} .
popd

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p -m 755 $RPM_BUILD_ROOT%{catrustdir}/source/anchors/

install -p -m 644 %{SOURCE1} $RPM_BUILD_ROOT%{catrustdir}/source/anchors/ETHZRootCA2020.crt
install -p -m 644 %{SOURCE2} $RPM_BUILD_ROOT%{catrustdir}/source/anchors/ETHZIssuingCA2020.crt
install -p -m 644 %{SOURCE3} $RPM_BUILD_ROOT%{catrustdir}/source/anchors/DigiCertGlobalRootCA.pem
install -p -m 644 %{SOURCE4} $RPM_BUILD_ROOT%{catrustdir}/source/anchors/DigiCertTLSRSASHA2562020CA1-1.pem
install -p -m 644 %{SOURCE5} $RPM_BUILD_ROOT%{catrustdir}/source/anchors/DigiCertGlobalRootG2.pem
install -p -m 644 %{SOURCE6} $RPM_BUILD_ROOT%{catrustdir}/source/anchors/DigiCertGlobalG2TLSRSASHA2562020CA1.pem

mkdir -p -m 755 $RPM_BUILD_ROOT%{_datadir}/doc/%{name}-%{version}
install -p -m 644 %{SOURCE0} $RPM_BUILD_ROOT%{_datadir}/doc/%{name}-%{version}/README

%clean
rm -rf $RPM_BUILD_ROOT

%post
%{_bindir}/update-ca-trust


%files
%defattr(-,root,root,-)

%dir %{catrustdir}
%dir %{catrustdir}/source
%dir %{catrustdir}/source/anchors
%{catrustdir}/source/anchors/ETHZRootCA2020.crt
%{catrustdir}/source/anchors/ETHZIssuingCA2020.crt
%{catrustdir}/source/anchors/DigiCertGlobalRootCA.pem
%{catrustdir}/source/anchors/DigiCertTLSRSASHA2562020CA1-1.pem
%{catrustdir}/source/anchors/DigiCertGlobalRootG2.pem
%{catrustdir}/source/anchors/DigiCertGlobalG2TLSRSASHA2562020CA1.pem

%{_datadir}/doc/%{name}-%{version}/README


%changelog
*Tue Mar 23 2021 Bengt Giger <bgiger@ethz.ch> - 2023.1-1
- Updated DigiCert G1 -> G2
*Tue Mar 23 2021 Bengt Giger <bgiger@ethz.ch> - 2022.1-1
- Added DigiCert
*Tue Mar 23 2021 Bengt Giger <bgiger@ethz.ch> - 2021.1-1
- Initial build
